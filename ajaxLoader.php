<?php
declare(strict_types=1);

include('loader.php');
$subscription = new subscription();
$action = $_REQUEST['action'];
$user   = $_REQUEST['user_id'];
$search = $_REQUEST['search'] ?? null;
$sort   = json_decode($_REQUEST['sort']??"", true) ?? [];

switch ($action) {
    case 'getShows':
        echo json_encode($subscription->getSubscriptions($user));
    break;
    case 'getEpisodes':
        $show       = $_REQUEST['show_id'];
        $showHidden = ($_REQUEST['hidden'] === "true");
        echo json_encode(
            $subscription->getEpisodes(
                $user,
                $show,
                $search,
                $sort,
                $showHidden
            )
        );
    break;
    case 'addShow':
        $show       = $_REQUEST['show_id'];
        $feeds      = $subscription->getFeedList($user);
        $feeds[]    = $show;
        echo $subscription->updateSubscriptions($user,$feeds);
    break;
    case 'updatePlayedTime':
        $episode    = (string) $_REQUEST['episode_id'];
        $playedTime = (float) $_REQUEST['played_time'];
        $hide       = (bool) ($_REQUEST['hidden'] === "true");

        $subscription->updatePlayTime($user, $episode, $playedTime, $hide);
    break;
    case 'removeShow':
        $show       = $_REQUEST['show_id'];
        $subscription->removeShow($user, $show);
    break;
    case 'refresh':
        echo $subscription->updateEpisodes($user);
    break;
}
