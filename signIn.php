<?php
include('loader.php');
?><!DOCTYPE>
<html lang="en">
<head>
    <title>PodPlay</title>
    <link rel="apple-touch-icon" sizes="180x180" href="media/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="media/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="media/favicon-16x16.png">
    <link rel="manifest" href="media/site.webmanifest">
    <meta charset="utf-8"/>
    <meta http-equiv='Content-Type' content='text/html; charset=utf-8;'/>
    <meta name="viewport" content="width=device-width, initial-scale=1"/>
    <script src="https://accounts.google.com/gsi/client" async defer></script>
    <script>
      let currentUser;
      function onSuccess(googleUser) {
        //const userProfile = parseJwt(googleUser.credential);

        let xhr = new XMLHttpRequest();
        xhr.open('POST', 'validateToken.php');
        xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
        xhr.onload = function () {
          currentUser = xhr.responseText;
          currentUser = window.localStorage.setItem(
            'token',
            currentUser
          );
          top.location = '/';
        };

        console.log(googleUser);
        xhr.send('token=' + googleUser.credential);
      }
    </script>

    <style>
        @import url(//fonts.googleapis.com/css?family=Open+Sans);

        * {
            font-family: "Open Sans", verdana, arial, sans-serif;
            color: #DEDEDE;
        }

        body {
            background-color: #1a1a1a;
            overflow-x: hidden;
            margin: 0;
        }

        .wrapper {
            margin: 0 auto;
            display: flex;
            align-items: center;
            justify-content: center;
            height: 100vh
        }
    </style>
</head>
<body>
    <div class="wrapper">
        <div id="g_id_onload"
             data-client_id="785839189864-de3rre6bf50iqb0amahms7pp9vtjp24p.apps.googleusercontent.com"
             data-context="signin"
             data-callback="onSuccess"
             data-auto_select="true"
             data-itp_support="true">
        </div>
    </div>
</body>
</html>
