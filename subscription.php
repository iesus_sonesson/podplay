<?php
declare(strict_types=1);
class subscription
{
    public function __construct(
        protected $requestBuilder = new \podplay\api\RequestBuilder()
    )
    {
        ini_set('error_reporting', E_ALL & ~E_NOTICE & ~E_WARNING);
    }

    public function updateSubscriptions(
        string $userId,
        array $subscriptions
    ): string|bool
    {
        $showData = $subscriptions;
        $requestPayload = '';
        foreach ($showData as $subscription) {
            $agent = 'Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; SV1; .NET CLR 1.0.3705; .NET CLR 1.1.4322)';
            $ch    = curl_init();
            curl_setopt($ch, CURLOPT_VERBOSE, false);
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_USERAGENT, $agent);
            curl_setopt($ch, CURLOPT_URL,       $subscription);
            $rssURI = curl_exec($ch);
            $rssObject = simplexml_load_string(
                $rssURI,
                'SimpleXMLElement',
                LIBXML_NOCDATA
            );

            if (!$rssObject) {

                continue;
            }

            $payload = json_decode(
                json_encode(
                    $rssObject
                ), true
            );

            unset($payload['channel']['item']);
            $payload['channel']['rssFeed']    = $subscription;
            $payload['channel']['id']         = sha1($subscription);
            $payload['channel']['user']['id'] = $userId;
            $payload                          = array_map('array_filter', $payload);
            $indexUpdate = [
                "update" => [
                    "_index" => "podplay_show",
                    "_type"  => "show",
                    "_id"    => sha1($subscription) . "_UID_" . $userId,
                ]
            ];
            $requestPayload .= json_encode($indexUpdate, JSON_FORCE_OBJECT) . "\n";
            $requestPayload .= json_encode(
                    [
                        'doc' => $payload,
                        'doc_as_upsert' => true
                    ]
                    , JSON_FORCE_OBJECT) . "\n";
        }


        $response = $this->requestBuilder->makeRequest(
            '_bulk',
            'show',
            'podplay_show',
            [],
            'post',
            'application/x-ndjson',
            $requestPayload
        );
        $this->updateEpisodes($userId);

        return $response;
    }

    public function removeShow(
        string $user,
        string $show
    ): string|bool
    {
        return $this->requestBuilder->makeRequest(
            $show . "_UID_" . $user,
            'show/',
            'podplay_show',
            [],
            'delete',
            '',
            ''
        );
    }

    public function updatePlayTime(
        string $user,
        string $episode,
        float $playedTime,
        bool $hide = false
    ): string|bool
    {
        $payload = [
            "doc" => [
                'playedTime' => $playedTime,
                'hidden' => $hide
            ]
        ];

        return $this->requestBuilder->makeRequest(
            '_update',
            'episode/' . $episode . "_UID_" . $user,
            'podplay',
            [],
            'post',
            'application/json',
            json_encode($payload, JSON_FORCE_OBJECT)
        );
    }

    public function updateEpisodes(
        string $userId
    ): string|bool
    {
        $subscriptions  = $this->getFeedList($userId);
        $requestPayload = '';
        foreach ($subscriptions as $index => $subscription) {
            $agent = 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/115.0.0.0 Safari/537.36';
            $ch    = curl_init();
            curl_setopt($ch, CURLOPT_VERBOSE, false);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
            curl_setopt($ch, CURLOPT_USERAGENT, $agent);
            curl_setopt($ch, CURLOPT_REFERER, '');
            curl_setopt($ch, CURLOPT_URL,       $subscription);
            $rssURI = curl_exec($ch);
            if (!curl_errno($ch)) {
              $info = curl_getinfo($ch);
              $httpStatusCode = $info['http_code'];
            }
            
            $rssObject = simplexml_load_string(
                $rssURI,
                'SimpleXMLElement',
                LIBXML_NOCDATA
            );

            if (!$rssObject) {
                if (php_sapi_name() == "cli") {
                    echo $rssURI."\n";
                }

                continue;
            }

            $showData = json_decode(
                json_encode(
                    $rssObject
                ), true
            );

            $episodes = $showData['channel']['item'];
            foreach ($episodes as $episode) {
                $episode['_podplayer_id'] = sha1($episode['guid']);
                $episode['_show_id']      = sha1($subscription);
                $episode['user']['id']    = $userId;
                $indexUpdate = [
                    "update" => [
                        "_index" => "podplay",
                        "_type"  => "episode",
                        "_id"    => $episode['_podplayer_id'] . "_UID_" . $userId,
                    ]
                ];
                $requestPayload .= json_encode($indexUpdate, JSON_FORCE_OBJECT) . "\n";
                $requestPayload .= json_encode(
                        [
                            'doc' => $episode,
                            'doc_as_upsert' => true
                        ]
                        , JSON_FORCE_OBJECT) . "\n";
            }

            if (php_sapi_name() == "cli") {
                $httpStatusCode = $httpStatusCode ?? '';
                echo "updated $httpStatusCode $subscription \n";
            }

        }

        if (!empty($requestPayload)) {
            return $this->requestBuilder->makeRequest(
                '_bulk',
                'episode',
                'podplay',
                [],
                'post',
                'application/x-ndjson',
                $requestPayload
            );
        }

        return false;
    }

    public function getFeedList(
        string $userId
    ): array
    {
        $shows = $this->getSubscriptions($userId);
        $feed = [];
        foreach ($shows as $show) {
            $feed[] = $show['channel']['rssFeed'];
        }

        return $feed;
    }

    public function getSubscriptions(
        string $userId
    ): array
    {
        $feed = [];
        $query = [
            "query" => [
                "term" => [
                    "channel.user.id.keyword" => [
                        "value" => (string)$userId
                    ]
                ]
            ],
            "from" => 0,
            "size" => 500
        ];
        $query = json_encode($query, JSON_FORCE_OBJECT);
        $showData = $this->requestBuilder->makeRequest(
            '_search',
            'show',
            'podplay_show',
            [],
            'GET',
            'application/json',
            $query
        );

        $showData = json_decode((string)$showData, true);
        $shows = $showData['hits']['hits'];
        foreach ($shows as $show) {
            $feed[] = $show['_source'];
        }

        return $feed;
    }

    public function getEpisodes(
        string $userId,
        string $showId,
        string $searchString = null,
        array $sort = [],
        bool $displayHidden = false
    ): array
    {
        $episodes = [];
        $query = [
            "query" => [
                "bool" => [
                    "must" => [
                        [
                            "term" => [
                                "user.id.keyword" => [
                                    "value" => $userId
                                ]
                            ]
                        ],
                        [
                            "term" => [
                                "_show_id.keyword" => [
                                    "value" => $showId
                                ]
                            ]
                        ],
                    ]
                ]
            ],
            "from" => 0,
            "size" => 2500
        ];

        if ($searchString) {
            $wildcardSearch = "*{$searchString}*";
            $query['query']['bool']['must'][]['bool']['should'][] = [
                [
                    "wildcard" => [
                        "title" => [
                            'value' => $wildcardSearch
                        ]
                    ]
                ],
                [
                    "match_phrase_prefix" => [
                        "title" => $searchString
                    ]
                ]
            ];
        }
        if (!$displayHidden) {
            $query['query']["bool"]["must_not"] = [
                [
                    "term" => [
                        "hidden.keyword" => [
                            "value" => "true"
                        ]
                    ]
                ]
            ];
        }
        if ($sort) {
            $query['sort'] = [
                $sort['field'] => $sort['order']
            ];
        }

        $query = json_encode($query);

        $episodeData = $this->requestBuilder->makeRequest(
            '_search',
            'episode',
            'podplay',
            [],
            'GET',
            'application/json',
            $query
        );

        $episodeData = json_decode((string)$episodeData, true);
        $extractEpisodes = $episodeData['hits']['hits'];
        foreach ($extractEpisodes as $episode) {
            $episodes[] = $episode['_source'];
        }

        return $episodes;
    }

    public function reindex(
    ): void
    {
        $users = [];
        $query = [
            "query" => [
                "match_all" => []
            ]
        ];
        $query = json_encode($query, JSON_FORCE_OBJECT);
        $showData = $this->requestBuilder->makeRequest(
            '_search',
            'show',
            'podplay_show',
            [],
            'GET',
            'application/json',
            $query
        );

        $showData = json_decode((string)$showData, true);
        $shows = $showData['hits']['hits'];
        foreach ($shows as $show) {
            $users[$show['_source']['channel']['user']['id']]
                = $show['_source']['channel']['user']['id'];
        }
        foreach ($users as $user) {
            $this->updateEpisodes($user);
        }
    }
}
