<?php
include('loader.php');
if (isset($_GET['GoogleToken'])) {
   $_SESSION['googleToken'] = $_GET['GoogleToken'];
    header('Location: /');
}
if (!isset($_SESSION['googleToken'])) {
    header('Location: signIn.php');
    exit;
}
$subscription = new subscription;
$showData     = $subscription->getSubscriptions((string)$_SESSION['googleToken']);
$currentUser  = $_SESSION['googleToken'];
?><!DOCTYPE>
<html lang='en'>
<head>
    <title>Fox Podcast Player</title>
    <link rel="apple-touch-icon" sizes="180x180" href="media/apple-touch-icon.png?hash=<?=sha1_file('media/apple-touch-icon.png');?>">
    <link rel="icon" type="image/png" sizes="32x32" href="media/favicon-32x32.png?hash=<?=sha1_file('media/favicon-32x32.png');?>">
    <link rel="icon" type="image/png" sizes="16x16" href="media/favicon-16x16.png?hash=<?=sha1_file('media/favicon-16x16.png');?>">
    <link rel="manifest" href="media/site.webmanifest?hash=<?=sha1_file('./media/site.webmanifest');?>">
    <meta charset="utf-8"/>
    <meta name="theme-color" content="#1a1a1a">
    <meta name="description" content="podcast player">
    <meta http-equiv='Content-Type' content='text/html; charset=utf-8;'/>
    <meta name="viewport"
          content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no"/>
    <script src="//www.gstatic.com/cv/js/sender/v1/cast_sender.js?loadCastFramework=1"></script>
    <meta name="google-signin-client_id"
    content="785839189864-de3rre6bf50iqb0amahms7pp9vtjp24p.apps.googleusercontent.com">
    <script src="resources/scripts.js?hash=<?=sha1_file('resources/scripts.js');?>"></script>
    <script src="//apis.google.com/js/platform.js"></script>
<script>
    if (typeof window.flags == 'undefined') {
      window.flags = {};
    }
    window.flags.manageMode  = false;
    window.flags.currentUser = '<?=$currentUser?>';
      /*
      window.__onGCastApiAvailable = function(isAvailable){
        if(! isAvailable){
          return false;
        }
        var castContext = cast.framework.CastContext.getInstance();
        castContext.setOptions({
          autoJoinPolicy: chrome.cast.AutoJoinPolicy.ORIGIN_SCOPED,
          receiverApplicationId: chrome.cast.media.DEFAULT_MEDIA_RECEIVER_APP_ID
        });
      };
      */
    /*if ('serviceWorker' in navigator) {
      window.addEventListener('load', function() {
          navigator.serviceWorker.register('/serviceWorker.js.php?hash=<?=sha1_file('serviceWorker.js.php')?>').then(function(registration) {
          // Registration was successful
          console.log('ServiceWorker registration successful with scope: ', registration.scope);
        }, function(err) {
          // registration failed :(
          console.log('ServiceWorker registration failed: ', err);
        });
      });
    }*/
    </script>
    <link rel="stylesheet" href="resources/style.css?hash=<?=sha1_file('resources/style.css'); ?>"/>
</head>
<body>
<header>
    <nav>
        <label for="autoPlay">
            <a>
                Auto&nbsp;play
                <input class="savedSetting"
                       onchange="saveState(this)"
                       type="checkbox"
                       id="autoPlay"
                />
                <span class="checkbox">&nbsp;</span>
            </a>
        </label>
        <a href="javascript:addShow()">+&nbsp;Add&nbsp;feed</a>
        <a href="javascript:toggleManageMode();">
            Manage
        </a>
        <label for="displayHidden">
            <a>
                Show&nbsp;hidden
                <input class="savedSetting"
                       onchange="saveState(this);loadEpisodes();"
                       type="checkbox"
                       id="displayHidden"
                />
                <span class="checkbox">&nbsp;</span>
            </a>
        </label>
        <label for="sort">
            <a>
                Sort
                <select class="Sort"
                        onchange="sort(this);"
                        id="searchBox"
                >
                    <option value="">Published date ↑</option>
                    <option value="reverse">Published date ↓</option>
                    <option value='{"field": "title.keyword", "order": "asc"}'>Title ↑</option>
                    <option value='{"field": "title.keyword", "order": "desc"}'>Title ↓</option>
                </select>
            </a>
        </label>
        <label for="search">
            <a>
                Search
                <input class="search"
                       onkeyup="search(this);"
                       type="text"
                       id="searchBox"
                />
            </a>
        </label>
    </nav>
</header>
<section class="show-wrapper">
    <?php foreach ($showData as $show) :?>
        <?php
        $episodeCount = count(
            $subscription->getEpisodes(
                $currentUser,
                $show['channel']['id'],
                "",
                [],
                false
            )
        );
        $showImage = $show['channel']['image']['url'];
        ?>
        <div class="show"
             data-show-id="<?= $show['channel']['id'] ?>"
             data-show-title="<?= $show['channel']['title']?>"
             data-show-image="<?= $showImage; ?>"
             data-show-episode-count="<?= $episodeCount ?>"
        >
            <a href="javascript:switchShow('<?= $show['channel']['id'] ?>');">
                <span class="episode-count"><?= $episodeCount ;?></span>
                <img
                    src="<?= $showImage; ?>"
                    alt="<?= $show['channel']['image']['title']; ?>"
                >

                <div><?= $show['channel']['title']; ?></div>
            </a>
        </div>
    <?php endforeach; ?>
</section>
<section id="controls">
    <div id="current-track">&nbsp;</div>
    <div class="audio-player">
        <div id="play-btn" onclick="playToggle()"></div>
        <div class="audio-wrapper" id="player-container" href="javascript:;">
            <audio id="player"></audio>
        </div>
        <div class="player-controls scrubber">
                <span id="seek-obj-container">
                  <progress id="seek-obj" value="0" max="1"></progress>
                </span>
            <small style="float: left; position: relative; top: 10px; left: 40px;" id="start-time"></small>
            <small style="float: right; position: relative; top: 10px; left: -40px;" id="end-time"></small>
        </div>
    </div>
    <div id="current-description">&nbsp;</div>
    <button
      style="
        background: transparent;
        max-width: 64px;
        border: none;
      "
      is='google-cast-button'
    ></button>
</section>
<section class="episodes">
    <ul>
    </ul>
</section>
</body>
</html>
