window.addEventListener('load', function () {
  if ('Notification' in window) {
    let grantNotificationsPermission = function () {
      if (window.Notification.permission !== "granted") {
        window.Notification.requestPermission();
      }
    }();
  }

  if (show = localStorage.getItem('currentShow')) {
    switchShow(show);
  }

  let settingsCheckboxes = document.querySelectorAll(".savedSetting");
  settingsCheckboxes.forEach(function(element){
    let key =element.id;
    if (localStorage.getItem(key) == "true") {
      element.checked = true;
      element.dispatchEvent(new Event('change'));
    }
  });

  initProgressBar();
},false);

let isPlaying = false;

(function updatePlayedTime() {
  setInterval(
    function() {
      if (isPlaying) {
        let player   = document.getElementById('player');
        let hide = false;
        if (player.duration-60 <= player.currentTime) {
          hide = true;
        }

        updatePlayedTimeForEpisode(
          window.flags.currentUser,
          window.flags.currentEpisode,
          player.currentTime,
          hide
        );
      }
    }, 15000);

  /**
   * Check every second if the audio is playing,
   *   set playbutton state accordingly.
   */
  setInterval(
    function() {
      let player   = document.getElementById('player');
      let button   = document.getElementById('play-btn');
      let autoPlay = document.getElementById("autoPlay").checked;
      if (player.paused !== false) {
        isPlaying = false;
        button.className = '';
        if (player.duration <= Math.ceil(player.currentTime) && autoPlay) {
          playNext();
        }
      } else {
        isPlaying = true;
        button.className = 'pause';
      }
    },
    1000
  );

  // Reload episode count
  setInterval(
    function(){
      loadSubscriptions();
    },
    (1000*60)*15 //15 minutes
  );
})();

function playNext() {
  let button      = document.getElementById('play-btn');
  let nowPlaying  = window.flags.currentEpisode;
  let nextEpisode = document.querySelector('.episodes li[data-id="'+ nowPlaying +'"] + li');
  let showId      = window.flags.currentShow;
  let show        = document.querySelector('.show[data-show-id="'+ showId +'"]');

  if (nextEpisode) {
    nextEpisode.click();
    button.click();

    sendNotification(
      show.getAttribute('data-show-title'),
      nextEpisode.getAttribute('data-title'),
      show.getAttribute('data-show-image')
    );
  }
  loadSubscriptions();
}

function saveState(el) {
  let state = el.checked;
  localStorage.setItem(el.id, state);
}

function search(el) {
  window.flags.searchText = el.value.toLowerCase();
  loadEpisodes();
}

function sort(el) {
  window.flags.sort = el.value;

  if (!el.value) {
    delete window.flags.sort;
  }

  loadEpisodes();
}

function switchShow(showId) {
  window.flags.currentShow = showId;
  localStorage.setItem('currentShow', showId);
  loadEpisodes();
}

function loadEpisodes() {
  let xhr = new XMLHttpRequest();
  xhr.open('POST', 'ajaxLoader.php');
  xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
  xhr.onload = function () {
    window.episodeList = JSON.parse(xhr.response);
    if (typeof window.flags.sort === "undefined") {
      window.episodeList.sort(function (a, b) {
        if (a.title === b.title) return 0;
        if (a.title < b.title) return -1;
        if (a.title > b.title) return +1;
      });
      renderEpisodes(episodeList);
      window.episodeList.sort(function (a, b) {
        let ad = new Date(a['pubDate']);
        let bd = new Date(b['pubDate']);
        if (bd === ad) return 0;
        if (bd > ad) return -1;
        if (bd < ad) return +1;
      });
    } else if(window.flags.sort === "reverse") {
      window.episodeList.sort(function (a, b) {
        if (a.title === b.title) return 0;
        if (a.title < b.title) return +1;
        if (a.title > b.title) return -1;
      });
      window.episodeList.sort(function (a, b) {
        let ad = new Date(a['pubDate']);
        let bd = new Date(b['pubDate']);
        if (bd === ad) return 0;
        if (bd > ad) return +1;
        if (bd < ad) return -1;
      });
    }

    renderEpisodes(episodeList);
    autoSelectEpisode();
  };

  if (!window.flags.searchText) {
    window.flags.searchText = "";
  }

  xhr.send('action=getEpisodes&'+
    'show_id=' + window.flags.currentShow +
    '&user_id='+ window.flags.currentUser +
    '&search='+ window.flags.searchText +
    '&sort='+ window.flags.sort +
    '&hidden=' + document.getElementById("displayHidden").checked
  );
}

function loadSubscriptions() {
  let xhr = new XMLHttpRequest();
  xhr.open('POST', 'ajaxLoader.php');
  xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
  xhr.onload = function () {
    window.subscriptionList = JSON.parse(xhr.response);

    renderShows(subscriptionList);
  };

  xhr.send('action=getShows&user_id='+ window.flags.currentUser);
}

function updatePlayedTimeForEpisode(userId, episodeId, playedTime, hide) {
  let xhr = new XMLHttpRequest();
  xhr.open('POST', 'ajaxLoader.php');
  xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
  xhr.send('action=updatePlayedTime'+
    '&episode_id=' + episodeId +
    '&user_id='    + userId +
    '&played_time='+ playedTime +
    '&hidden='     + hide
  );
}

function autoSelectEpisode() {
  let currentEpisode;
  let episode;
  if (currentEpisode = localStorage.getItem('currentEpisode')) {
    let episodeData = window.episodeList;
    if (!window.episodeList) {

      return;
    }

    episodeData.forEach(function(ep) {
      if (typeof ep !== "undefined") {
        if (ep._podplayer_id == currentEpisode) {
          episode = ep;
        }
      }
    });
    if (!episode) {

      return;
    }

    playFile(
      episode['enclosure']['@attributes']['url'],
      episode['enclosure']['@attributes']['type'],
      episode['title'],
      episode['_podplayer_id']
    );

    if (episode.playedTime != null && isPlaying !== true) {
      player.currentTime = episode.playedTime;
    }
  }
}

function renderEpisodes(episodeList) {
  let ul = document.querySelector('.episodes > ul');
  let li;
  while (ul.firstChild) {
    ul.removeChild(ul.firstChild);
  }
  if (episodeList.length <= 0) {

    return;
  }
  episodeList.forEach(function(episode) {
    let rd = new Date(episode['pubDate']);
    li = document.createElement('li');
    li.onclick = function () {
        playFile(episode['enclosure']['@attributes']['url'],
          episode['enclosure']['@attributes']['type'],
          episode['title'],
          episode['_podplayer_id']);
    };
    li.setAttribute('data-id',    episode['_podplayer_id']);
    li.setAttribute('data-title', episode['title']);
    li.setAttribute('data-playedTime', parseFloat(episode['playedTime']));
    if (parseFloat(episode['playedTime']) > 0) {
      li.className += " initiated";
    }
    if (parseFloat(episode['playedTime']) === 1) {
      li.className += " manuallyHidden";
    }
    if (
      episode['hidden'] === "true" ||
      episode['hidden'] === true
    ) {
      li.className += " hidden";
    }
    li.innerText =  rd.toLocaleString('sv-SE').substr(0,10) +' '+ episode['title'];
    ul.appendChild(li);
  });
  let episodeCount = document.querySelector(
    '.show[data-show-id="'+ window.flags.currentShow +'"] .episode-count'
  );
  episodeCount.innerText = episodeList.length;
  toggleManageMode();
}

function renderShows(showList) {
  let wrapper = document.querySelector('.show-wrapper');
  while (wrapper.firstChild) {
    wrapper.removeChild(wrapper.firstChild);
  }
  showList.forEach(function(show) {
    let episodeCount=1, count, image, title, link, div;

    count = document.createElement('span');
    count.setAttribute('class', "episode-count");
    count.innerText = episodeCount;

    image = document.createElement('img');
    image.setAttribute('src', show['channel']['image']['url']);
    image.setAttribute('alt', show['channel']['image']['title']);

    title = document.createElement('div');
    title.innerText = show['channel']['title'];

    link  = document.createElement('a');
    link.setAttribute('href', "javascript:switchShow('"+ show['channel']['id'] +"');");
    link.appendChild(count);
    link.appendChild(image);
    link.appendChild(title);

    div  = document.createElement('div');
    div.appendChild(link);
    div.setAttribute('class',                   'show');
    div.setAttribute('data-show-id',            show['channel']['id']);
    div.setAttribute('data-show-title',         show['channel']['title']);
    div.setAttribute('data-show-image',         show['channel']['image']['url']);
    div.setAttribute('data-show-episode-count', episodeCount);

    wrapper.appendChild(div);
    updateEpisodeCount(show['channel']['id']);
  });
}

function updateEpisodeCount(showId) {
  let xhr = new XMLHttpRequest();
  xhr.open('POST', 'ajaxLoader.php');
  xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
  xhr.onload = function () {
    let result   = JSON.parse(xhr.response);
    let episodes = result.filter(
      function(user) {
        return user._show_id === showId;
      }
    );
    let episodeCount = episodes.length;
    let updateEl = document.querySelector('.show-wrapper .show[data-show-id="'+showId+'"]');
    updateEl.setAttribute('data-show-episode-count', episodeCount);
    document.querySelector('.show[data-show-id="'+showId+'"] .episode-count').innerText = episodeCount;
  };

  xhr.send('action=getEpisodes&'+
    'show_id=' + showId +
    '&user_id='+ window.flags.currentUser +
    '&hidden=' + document.getElementById("displayHidden").checked +
    '&search=' + window.flags.searchText
  );

  return 0;
}

function playFile(uri, type, title, id) {
  if (window.flags.currentEpisode === id) {

    return;
  }

  if (isPlaying === true) {
    if (!confirm('Are you sure?')) {
      return;
    }
  }

  isPlaying = false;
  let episode;
  let episodeData = window.episodeList;
  for (i = 0; i <= episodeData.length; i++) {
    if (episodeData[i]._podplayer_id === id) {
      episode = episodeData[i];
      break;
    }
  }

  localStorage.setItem('currentEpisode', episode._podplayer_id);
  window.flags.currentEpisode = episode._podplayer_id;

  document.title = 'PodPlay - ' + title;
  document.getElementById('current-track').innerText = title;
  document.getElementById('current-description').innerHTML = episode.description;

  let player = document.getElementById('player');
  if (player) {
    player.pause();
    document.getElementById('play-btn').removeEventListener('click',function () {
      togglePlay()
    });
    player.parentNode.removeChild(player);
  }

  player = document.createElement("audio");

  if (player.canPlayType(type)) {
    player.setAttribute("id", 'player');
    player.setAttribute("src", uri);
    player.setAttribute("ontimeupdate", "initProgressBar()");
    document.getElementById('player-container').appendChild(player);
  }

  /*
  let stateChanged = cast.framework.CastContextEventType.CAST_STATE_CHANGED;
  let castContext = cast.framework.CastContext.getInstance();

  castContext.addEventListener(stateChanged, function(event) {
    let castSession = castContext.getCurrentSession();
    let media = new chrome.cast.media.MediaInfo(uri, type);
    let request = new chrome.cast.media.LoadRequest(media);
    castSession && castSession
      .loadMedia(request)
      .then(function(){
        console.log('Success');
      })
      .catch(function(error){
        console.log('Error: ' + error);
      });
  });
  castContext.fireEvent("stateChanged");
  */

  player.load();
  if (episode.playedTime != null) {
    player.currentTime = episode.playedTime;
  }
  initProgressBar();
  setMetaData(episode, player);
}

function setMetaData(episode, player) {
  let showId = window.flags.currentShow;
  let show   = document.querySelector('.show[data-show-id="'+ showId +'"]');
  if ("mediaSession" in navigator) {
    let skipTime = 10;
    navigator.mediaSession.setActionHandler('seekbackward', function() {
      player.currentTime = Math.max(player.currentTime - skipTime, 0);
    });
    navigator.mediaSession.setActionHandler('seekforward', function() {
      player.currentTime = Math.min(player.currentTime + skipTime, player.duration);
    });
    navigator.mediaSession.setActionHandler('play', function() {
      player.play();
    });
    navigator.mediaSession.setActionHandler('pause', function() {
      player.pause();
    });
    navigator.mediaSession.metadata = new MediaMetadata({
      title: episode.title,
      artist: show.getAttribute('data-show-title'),
      album: show.getAttribute('data-show-title'),
      artwork: [{ src: show.getAttribute('data-show-image') }]
    });
  }
}

function initProgressBar() {
  let player       = document.getElementById('player');
  let length       = isNaN(player.duration) ? 0 : player.duration;
  let current_time = player.currentTime;

  // calculate total length of value
  let totalLength = calculateTotalValue(length)
  document.getElementById("end-time").innerHTML = totalLength;

  // calculate current value time
  let currentTime   = calculateCurrentValue(current_time);
  document.getElementById("start-time").innerHTML = currentTime;
  let progressbar   = document.getElementById('seek-obj');
  if (current_time <= 0 || length <= 0) {
    progressbar.value = 0;
  } else {
    length = (length==0) ? 1: length;
    progressbar.value = (current_time / length);
  }
  progressbar.addEventListener("click", seek);

  if (player.currentTime === player.duration) {
    document.getElementById('play-btn').className = "";
  }

  function seek(event) {
    let percent = event.offsetX / this.offsetWidth;
    player.currentTime = percent * length;
    progressbar.value = percent / 100;
    if ("ccPlayerController" in window) {
      window.ccPlayerController.seek(player.currentTime);
    }
  }
}

function playToggle() {
    let player = document.getElementById('player');
    let button = document.getElementById('play-btn');    
    
    if (player.paused !== false) {
      let playPromise = player.play();
      button.className = 'pause';
    } else {
      button.className = '';
      player.pause();
    }

    if (typeof playPromise !== "undefined") {
      playPromise.then(_ => {
          console.log('started');
      })
    .catch(error => {
          console.log('error starting');
      });
    }

    if ("ccPlayerController" in window) {
      window.ccPlayerController.playOrPause();
    }
}

function calculateTotalValue(length) {
  let hours   = Math.floor(length / 3600);
  let minutes = Math.floor((length - (hours*3600)) / 60),
    seconds_int = length - minutes * 60,
    seconds_str = seconds_int.toString(),
    seconds = seconds_str.substr(0, 2);

    if (isNaN(hours))   { hours = 0;   }
    if (isNaN(minutes)) { minutes = 0; }
    if (isNaN(seconds)) { seconds = 0; }

    minutes = (minutes < 10) ? "0"+ minutes : minutes;
    seconds = (seconds < 10) ? "0"+ seconds : seconds;

  return hours + ':' + minutes + ':' + seconds
}

function calculateCurrentValue(currentTime) {
  let current_hour = parseInt(currentTime / 3600) % 24,
    current_minute = parseInt(currentTime / 60) % 60,
    current_seconds_long = currentTime % 60,
    current_seconds = current_seconds_long.toFixed();

  return current_hour +
        ':' +
        (current_minute < 10 ? "0" + current_minute : current_minute) +
        ":" +
        (current_seconds < 10 ? "0" + current_seconds : current_seconds);
}

function addShow() {
  let newUrl = prompt("Provide URL");
  if (newUrl != null) {
    let xhr = new XMLHttpRequest();
    xhr.open('POST', 'ajaxLoader.php');
    xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    xhr.onload = function () {
      top.location = '/';
    };
    xhr.send('action=addShow&show_id='+ newUrl + '&user_id='+ window.flags.currentUser);
  }
}

function removeShow(userId, showId) {
  let xhr = new XMLHttpRequest();
  xhr.open('POST', 'ajaxLoader.php');
  xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
  xhr.send('action=removeShow&show_id='+ showId + '&user_id='+ userId);
}

function toggleManageMode() {
  if (!window.flags.manageMode) {
    let shows = document.querySelectorAll('section.show-wrapper > .show > a');
    shows.forEach(function(show) {
      addControls(show, [
        {
          name: "refresh",
          action: (function() {
            let xhr = new XMLHttpRequest();
            xhr.open('POST', 'ajaxLoader.php');
            xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
            xhr.onload = function() {
              top.location = '/';
            };
            xhr.send('action=refresh&user_id='+ window.flags.currentUser);
            event.stopPropagation();
            event.preventDefault();
          })
        },
        {
          name: "delete",
          action: (function() {
            if (confirm('Are you sure?')) {
              removeShow(
                window.flags.currentUser,
                show.parentNode.getAttribute('data-show-id')
              );
              show.parentNode.removeChild(show);
              event.stopPropagation();
              event.preventDefault();
            }
          })
        }
      ]);
    });

    let episodes = document.querySelectorAll('section.episodes > ul > li');
    episodes.forEach(function(episode) {
      addControls(
        episode, [
          {
            name: "hide",
            action: (function () {
              updatePlayedTimeForEpisode(
                window.flags.currentUser,
                episode.getAttribute('data-id'),
                1,
                true
              );
              episode.parentNode.removeChild(episode);
              event.stopPropagation();
              event.preventDefault();
            })
          },
          {
            name: "reset",
            action: (function () {
              updatePlayedTimeForEpisode(
                window.flags.currentUser,
                episode.getAttribute('data-id'),
                0.0,
                false
              );
              event.stopPropagation();
              event.preventDefault();
            })
          },
        ]
      );
    });

    function addControls(element, list) {
      list.forEach(function(button) {
        let child = document.createElement('div');
        child.className  = 'manage-controls '+ button.name;
        child.innerText  = button.name;
        child.onclick    = button.action;
        element.appendChild(child);
      });
    }
    window.flags.manageMode = true;
  } else {
    let manageControls = document.querySelectorAll('.manage-controls');
    manageControls.forEach(function(control) {
      control.parentNode.removeChild(control);
    });
    window.flags.manageMode = false;
  }

  document.querySelectorAll('section.show-wrapper > .show').forEach(function(show){
    show.setAttribute('data-show-manage', window.flags.manageMode.toString());
  });
}

function sendNotification(showTitle, episodeTitle, icon) {
  if ('Notification' in window) {
    let notification = new Notification(
      episodeTitle, {
        icon: icon,
        body: 'Automatically changed episode to next episode (' + episodeTitle + ') on show ' + showTitle,
      }
    );
    notification.onclick = function () {
      notification.close();
    };
  }
}
