<?php
$type = $_GET['ct'] ?? 'audio/mpeg';
header('Content-Type: ' . urldecode($type));
$url = urldecode($_GET['url']);
$f = fopen($url, 'r');
// Read chunks maximum number of bytes to read
if (!$f) exit;
while (!feof($f)) {
    echo fread($f, 128);
    flush();
}
fclose($f);
